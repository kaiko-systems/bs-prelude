;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((projectile-project-compilation-cmd . "make compile")))
  (js-json-mode . ((js-indent-level . 2)
                    (tab-width . 2)))
  (json-mode . ((js-indent-level . 2)))
  (rescript-mode . ((projectile-project-compilation-cmd . "make compile")
                     (indent-tabs-mode . nil))))
