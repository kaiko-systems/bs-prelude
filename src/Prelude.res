@@uncurried
external identity: 'a => 'a = "%identity"

@deprecated
let uuid4: unit => string = %raw(`
  function () {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(
      /[018]/g,
      (c) => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  }
`)

%%private(external cast: 'a => 'b = "%identity")

@doc("The standard composition of two functions.")
let compose = (f, g, a) => a->g->f

module PromiseExported = {
  include Js.Promise2

  let thenResolve = async (promise, function) => function(await promise)
  let catchResolve = (promise, function) => promise->catch(x => x->function->resolve)

  @send external finally: (t<'a>, unit => unit) => t<'a> = "finally"

  @doc("Return a promise that rejects with an exception as an Error(e)")
  let rejectWithError = (ex: exn): t<result<'a, error>> => reject(ex)->catchResolve(e => Error(e))

  @doc("Execute `function(x)` with the value resolved by the promise.")
  let thenDo: (promise<'a>, 'a => unit) => unit = (promise, function) =>
    promise->thenResolve(function)->ignore
}

module Promises = {
  include PromiseExported

  @deprecated @doc("Fork a promise, returning both the resolved value and the previous promise.")
  let fork = (promise, function) => (promise->then(function), promise)

  @doc("Creates a promise that is resolved after some time (in milliseconds)
  have passed.")
  let ellapsed = (milliseconds: int) =>
    Js.Promise.make((~resolve, ~reject) => {
      reject->ignore
      let nothing = ()
      Js.Global.setTimeout(() => resolve(nothing)->ignore, milliseconds)->ignore
    })

  @doc("Transforms promises that fail into promises that always resolve with a
  `result<'a, 'b'>` payload.")
  let result = async p => {
    switch await p {
    | data => Ok(data)
    | exception error => await rejectWithError(error)
    }
  }

  /* Internal implementation of iter */
  let rec _iter = async (fns, results) =>
    switch fns {
    | list{} => results
    | list{fn, ...rest} =>
      let result = await fn()
      await _iter(rest, results->Js.Array2.concat([result]))
    }

  @doc("Execute all functions one after the other and resolve after all resolved.")
  let iter: array<unit => promise<'a>> => promise<array<'a>> = fns =>
    _iter(fns->Belt.List.fromArray, [])
}
include PromiseExported

module OptionExported = {
  @doc("Maps `Some(x)` to `(true, x)` and None to `(false, default)`")
  let defaultWithBool = (value, default) =>
    value->Belt.Option.mapWithDefault((false, default), v => (true, v))

  @doc("An alias to Belt.Option.flatMap")
  let maybe = Belt.Option.flatMap

  @doc("An alias to Belt.Option.getWithDefault")
  let default = Belt.Option.getWithDefault

  module Option = {
    include Belt.Option

    @doc("Call the funtion with the Some's payload.

    This is an alias to the undocumented Option.forEach, which sounds like
    iteration.
    ")
    let do: (option<'a>, 'a => unit) => unit = (opt, fn) => forEach(opt, fn)

    let thenMap = (promise: promise<option<'a>>, resolver: 'a => 'b) => {
      promise->then(result => map(result, resolver)->resolve)
    }
  }
}
include OptionExported

module Result = {
  include Belt.Result

  @doc("Maps a `Result.t<'a, 'b'> to 'c by applying fallback function default
  to a contained Error value, or function f to a contained Ok value.")
  let mapOrElse = (res: result<'a, 'b>, fn: 'a => 'c, deffn: 'b => 'c): 'c =>
    switch res {
    | Ok(value) => fn(value)
    | Error(err) => deffn(err)
    }

  @doc("Maps a `result<'a, 'b>` to `option<'a>`.")
  let ok = res =>
    switch res {
    | Ok(v) => Some(v)
    | _ => None
    }

  @doc("Maps a `result<'a, 'b>` to `option<'a>`; issues a 'Js.Console.warn'
  with the error.")
  let warn = res =>
    switch res {
    | Ok(v) => Some(v)
    | Error(msg) => {
        Js.Console.warn(msg)
        None
      }
    }

  @doc("Maps a `result<'a, 'b>` to `option<'a>`; issues a 'Js.Console.error'
  with the error.")
  let scream = res =>
    switch res {
    | Ok(v) => Some(v)
    | Error(msg) => {
        Js.Console.error(msg)
        None
      }
    }

  @doc("Maps a `result<'a, 'b>` to `option<'b>`.")
  let err = res =>
    switch res {
    | Error(v) => Some(v)
    | _ => None
    }

  @doc("Similar to 'map' but passes the Error")
  let mapError = (res: result<'a, 'b>, fn: 'b => 'c): result<'a, 'c> =>
    switch res {
    | Ok(value) => Ok(value)
    | Error(err) => Error(fn(err))
    }

  @doc("Similar to 'flatMap' but passes the Error")
  let flatMapError = (res: result<'a, 'b>, fn: 'b => result<'a, 'c>): result<'a, 'c> =>
    switch res {
    | Ok(value) => Ok(value)
    | Error(err) => fn(err)
    }

  @doc("Flip Ok to Error, and Error to Ok.")
  let flip = r =>
    switch r {
    | Ok(x) => Error(x)
    | Error(x) => Ok(x)
    }
}

module Dict = {
  include Js.Dict

  let has = (dict, key) => dict->get(key)->Belt.Option.isSome

  @doc("Same as Dict.map, but pipe-first")
  let mapValues = (dict: t<'a>, fn: 'a => 'b): t<'b> => map(value => value->fn, dict)

  @doc("Pass each value to `fn` and keep only the entries for which it doesn't return None.")
  let keepMap = (d: t<'a>, fn: 'a => option<'b>): t<'b> =>
    d
    ->entries
    ->Belt.Array.keepMap(((key, value)) => value->fn->Option.map(v => (key, v)))
    ->fromArray
}

module Array = {
  include Belt.Array

  @send external map: (t<'a>, @uncurry 'a => 'b) => t<'b> = "map"
  @send external mapWithIndex: (t<'a>, @uncurry ('a, int) => 'b) => t<'b> = "map"
  @send external reduce: (t<'a>, @uncurry ('b, 'a) => 'b, 'b) => 'b = "reduce"
  @send external reducei: (t<'a>, @uncurry ('b, 'a, int) => 'b, 'b) => 'b = "reduce"

  let fold = Belt.Array.reduce

  let chooseFrom = choices => choices->getUnsafe(Js.Math.random_int(0, choices->length))

  @send external join: (array<string>, string) => string = "join"
  @send external filter: (array<'a>, 'a => bool) => array<'a> = "filter"

  let includes = (items, something) => items->some(item => item == something)
  let isNotEmpty: array<'a> => bool = items => items->length->cast
  let isEmpty = items => !(items->isNotEmpty)

  let first = items => items->get(0)
  let firstUnsafe = items => items->getUnsafe(0)

  @doc("Return the first element in the provided array that satisfies the
  provided testing function. If no value satisfies the testing function,
  return None.

  See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find")
  @send
  external find: (array<'a>, 'a => bool) => option<'a> = "find"

  @deprecated @send external firstWhere: (array<'a>, 'a => bool) => option<'a> = "find"

  let flatMap = (items, function) => items->map(function)->concatMany

  let last = items => {
    if items->length > 0 {
      items->get(items->length - 1)
    } else {
      None
    }
  }
  let lastUnsafe = items => items->getUnsafe(items->length - 1)
  let keepSome = keepMap(_, identity)

  let sort = (items: array<'a>, key: 'a => 'b, ~reverse: bool=false) => {
    let sign = reverse ? -1 : 1
    items->Belt.SortArray.stableSortBy((a, b) => sign * Pervasives.compare(a->key, b->key))
  }

  let group = (items: array<'a>, getKey: 'a => string) => {
    let groups = Dict.empty()
    items->forEach(item => {
      let key = item->getKey
      groups->Dict.set(key, groups->Dict.get(key)->default([])->concat([item]))
    })
    groups
  }

  let groupBy = (items: array<'a>, getKey: 'a => 'b, ~id) => {
    let groups = Belt.HashMap.make(~id, ~hintSize=10)
    items->forEach(item => {
      let key = item->getKey
      groups->Belt.HashMap.set(key, groups->Belt.HashMap.get(key)->default([])->concat([item]))
    })
    groups
  }

  let groupByComparable = (items: array<'a>, getKey: 'a => 'b, ~id) => {
    let groups = Belt.MutableMap.make(~id)
    items->forEach(item => {
      let key = item->getKey
      groups->Belt.MutableMap.set(
        key,
        groups->Belt.MutableMap.get(key)->default([])->concat([item]),
      )
    })
    groups
  }

  let getter = (items, groupFunction) => {
    let items = items->group(groupFunction)
    key => items->Dict.get(key)->default([])
  }

  let getterWith = (items, grouper, ~id) => {
    let items = items->groupBy(grouper, ~id)
    key => items->Belt.HashMap.get(key)->default([])
  }

  let getterWithComparable = (items, grouper, ~id) => {
    let items = items->groupByComparable(grouper, ~id)
    key => items->Belt.MutableMap.get(key)->default([])
  }

  @doc("A safer version of sliceToEnd, it won't raise an exception when the
  offset is greater than the length of the array, instead return the empty
  array.")
  let tail = (items: array<'t>, offset: int): array<'t> =>
    switch items->sliceToEnd(offset) {
    | res => res
    | exception _ => []
    }

  @doc("Create a new array with the element at the end.")
  let append = (items, item) => items->concat([item])

  @doc("Create a new array with the element at the beginning.")
  let prepend = (items, item) => [item]->concat(items)

  @doc("Get the items in the array in chunks of at most the given size.  If
        size is less or equal than 0 return a single chunk.")
  let chunks: (array<'a>, int) => array<array<'a>> = (items, size) => {
    let count = items->Array.length
    if (size <= 0 && size >= count) || count == 0 {
      [items]
    } else {
      let pages = Js.Math.ceil(count->Belt.Int.toFloat /. size->Belt.Int.toFloat)
      /// We need makeBy here because otherwise the value [] would shared and
      /// not a distinct list for each page.
      let res = Belt.Array.makeBy(pages, _ => [])
      items->Belt.Array.reduceWithIndex(res, (res, item, index) => {
        let page = index / size
        res[page]->Js.Array2.push(item)->ignore
        res
      })
    }
  }

  module Int = {
    let sum: array<int> => int = reduce(_, (s, v) => s + v, 0)
  }

  module Float = {
    let sum: array<float> => float = reduce(_, (s, v) => s +. v, 0.0)
  }
}

module Int = {
  include Js.Int
  include Belt.Int
}

module Float = {
  include Js.Float
  include Belt.Float
}

module Set = Belt.Set
module List = Belt.List

module String = {
  include Js.String2

  @send external padStart: (string, int, string) => string = "padStart"

  let hasText = (text: option<string>) => text->default("")->trim->length > 0

  let default = (text: option<string>, default_: string) => {
    text
    ->maybe(x =>
      if Some(x)->hasText {
        Some(x)
      } else {
        None
      }
    )
    ->default(default_)
  }

  @doc("Indent each line of the TEXT with MANY spaces.")
  let indent = (text, many) =>
    text->split("\n")->Array.map(line => repeat(" ", many)->concat(line))->Array.join("\n")
}

module Bool = {
  @val external isTruish: 'a => bool = "Boolean"
  let fromMaybeString = value =>
    switch value {
    | Some("true") | Some("yes") => true
    | _ => false
    }

  let toString = value => value ? "true" : "false"
}

module ManyResults = {
  open Array
  include Result

  @doc("Given an array of results, return `Ok([...])` if all items are
  `Ok(...)`, or the first Error in the array.")
  let bail = (items: array<t<'t, 'e>>): t<array<'t>, 'e> =>
    items->reduce((res, nextitem) => nextitem->flatMap(i => res->map(r => r->concat([i]))), Ok([]))

  @doc("Similar to `bail` but items are only executed if no Error has happened before.")
  let bailU = (fns: array<unit => t<'t, 'e>>): t<array<'t>, 'e> =>
    fns->reduce(
      (res, nextitem) => res->flatMap(arr => nextitem()->map(i => arr->concat([i]))),
      Ok([]),
    )

  @doc("Execute each fn until one returns an Ok and return it, otherwise return default.")
  let rec stopU = (fns: array<unit => t<'t, 'e>>, default: t<'t, 'e>): t<'t, 'e> => {
    switch (fns->get(0), fns->tail(1)) {
    | (None, _) => default
    | (Some(fn), tail) =>
      switch fn() {
      | Error(_) => stopU(tail, default)
      | Ok(res) => Ok(res)
      }
    }
  }

  @doc("Partitions a list of results into two lists. All the Ok elements are
  extracted, in order, to the first component of the output. Similarly the
  Error elements are extracted to the second component of the output.")
  let partition = (items: array<t<'a, 'b>>): (array<'a>, array<'b>) =>
    items->reduce(((oks, errs), res) =>
      switch res {
      | Ok(v) => (oks->concat([v]), errs)
      | Error(v) => (oks, errs->concat([v]))
      }
    , ([], []))
}

@doc("This module allows to treat promises that resolve to a 'result' almost
like a result.

Bear in mind that the return type of all these functions is still a
promise.
")
module PromisedResult = {
  type t<'a, 'e> = promise<result<'a, 'e>>

  @doc("Unlift a promised result to normal promise that rejects with errors.")
  let unlift: t<'a, Promises.error> => promise<'a> = async pr => {
    switch await pr {
    | Ok(result) => result
    | Error(error) => await reject(error->cast)
    }
  }

  let map: (t<'a, 'b>, 'a => 'c) => t<'c, 'b> = async (p, fn) => Result.map(await p, fn)

  let mapWithDefault: (t<'a, 'e>, 'b, 'a => 'b) => promise<'b> = async (p, default, fn) =>
    Result.mapWithDefault(await p, default, fn)

  let flatMap: (t<'a, 'e>, 'a => result<'b, 'e>) => t<'b, 'e> = async (p, fn) =>
    Result.flatMap(await p, fn)

  let mapError: (t<'a, 'b>, 'b => 'c) => t<'a, 'c> = async (p, fn) => Result.mapError(await p, fn)

  let flatMapError: (t<'a, 'b>, 'b => result<'a, 'c>) => t<'a, 'c> = async (p, fn) =>
    Result.flatMapError(await p, fn)

  let bind: (t<'a, 'b>, 'a => t<'c, 'b>) => t<'c, 'b> = async (p, fn) =>
    switch await p {
    | Ok(a) => await fn(a)
    | Error(msg) => Error(msg)
    }

  @doc("Convert a promise resolving to result, to one that resolves to an
  option and issues a warning if the original promise resolved to an Error.")
  let warn: t<'a, 'b> => promise<option<'a>> = async p => Result.warn(await p)

  @doc("Convert a promise resolving to result, to one that resolves to an
  option and screams if the original promise resolved to an Error.")
  let scream: t<'a, 'b> => promise<option<'a>> = async p => Result.scream(await p)

  @doc("Convert a promise resolving to result, to one that resolves to an
  option discarding Errors.")
  let ok: t<'a, 'b> => promise<option<'a>> = async p => Result.ok(await p)

  @doc("Flip Ok to Error, and Error to Ok.")
  let flip: t<'a, 'e> => t<'e, 'a> = async r => Result.flip(await r)

  %%private(
    let rec _bailImple: (array<'a>, list<unit => t<'a, 'e>>) => t<array<'a>, 'e> = async (
      items,
      fns,
    ) =>
      switch fns {
      | list{} => Ok(items)
      | list{fn, ...fns} =>
        switch await fn() {
        | Ok(item) => await _bailImple(items->Belt.Array.concat([item]), fns)
        | Error(msg) => Error(msg)
        }
      }
    and _bail = (fns: array<unit => t<'a, 'e>>): t<array<'a>, 'e> =>
      _bailImple([], fns->List.fromArray)
  )

  @doc("Run each function in turn until one resolves to an Error, or all resolve to Ok.")
  let bail: array<unit => t<'a, 'e>> => t<array<'a>, 'e> = _bail

  @doc("Run each function in turn passing each Ok result to the next function.
  Stop as soon as a function returns Error.")
  let chain: ('a, array<'a => t<'a, 'e>>) => t<'a, 'e> = (default, fns) =>
    fns->Array.reduce(bind, resolve(Ok(default)))

  @doc("Execute the actions one by one and resolve to the first Ok
  result, if no action returns Ok, resolve to the last Error result.

  The only case where this resolves to None is when `actions` is the empty
  array.")
  let untilOk: array<unit => t<'a, 'e>> => t<option<'a>, 'e> = async actions => {
    switch await actions->Array.map(a => _ => a()->flip)->bail {
    | Error(r) => Ok(Some(r))
    | Ok([]) => Ok(None)
    | Ok(errors) => Error(errors->Array.getUnsafe(0))
    }
  }
}

exception NotImplementedError
exception AssertionError(string)
