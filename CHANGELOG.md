# Changes log

## 3.3.0

- Add 'Result.mapError' and 'Result.flatMapError'
- Add 'PromisedResult.mapError' and 'PromisedResult.flatMapError'.


## 3.2.0

- Add 'Result.scream' and 'PromisedResult.scream'

## 3.1.0 and older 

Sorry, too old history.
